


function CameraSim()
{
	var backgroundImage = $("#backgroundImage")[0];
	var foregroundImage = $("#foregroundImage")[0];
	var imageWidth = 2400;
	var imageHeight = 1600;

	var canvas = $("#canvas")[0];
	var ctx = canvas.getContext('2d');
	var canvasWidth = $("#canvas").width();
	var canvasHeight = $("#canvas").height();

	var distance = 3; 		// m
	var focalLength = 18;	// mm
	var aperture = 8;
	var shutter = 1.0/125;	// s
	var ISO = 200;
	var viewFinder = false;

	var blurRadius = 50;
	var baseZoom = (focalLength * 1e-3) / (distance - focalLength * 1e-3);
	var baseLight = shutter * ISO / (aperture * aperture);
	var baseISO = ISO;


	// Zmenit svetelnost snimku
	var changeBrightness = function (brightness) {
		var imageData = ctx.getImageData(0, 0, canvasWidth, canvasHeight);
		var pixels = imageData.data;

		var p = imageData.width * imageData.height;
		var pix = p*4, pix1 = pix + 1, pix2 = pix + 2;
		var add = brightness * 50;
		
		while (p--) {
			if ((pixels[pix-=4] = pixels[pix] + add) > 255)
				pixels[pix] = 255;

			if ((pixels[pix1-=4] = pixels[pix1] + add) > 255)
				pixels[pix1] = 255;

			if ((pixels[pix2-=4] = pixels[pix2] + add) > 255)
				pixels[pix2] = 255;
		}

		//ctx.clearRect(0, 0, canvasWidth, canvasHeight);  
		ctx.putImageData(imageData, 0, 0);  
	};

	// Pridat RGB sum
	// Zdroj: http://www.pixastic.com/lib/git/pixastic/actions/noise.js
	var addNoise = function (amount, strength) {
		var imageData = ctx.getImageData(0, 0, canvasWidth, canvasHeight);
		var pixels = imageData.data;

		amount = Math.max(0,Math.min(1,amount));
		strength = Math.max(0,Math.min(1,strength));

		var noise = 128 * strength;
		var noise2 = noise / 2;
		var w = imageData.width;
		var h = imageData.height;
		var w4 = w*4;
		var y = h;
		var random = Math.random;

		do {
			var offsetY = (y-1)*w4;
			var x = w;
			do {
				var offset = offsetY + (x-1)*4;
				if (random() < amount) {
					var r = pixels[offset] - noise2 + (random() * noise);
					var g = pixels[offset+1] - noise2 + (random() * noise);
					var b = pixels[offset+2] - noise2 + (random() * noise);

					if (r < 0 ) r = 0;
					if (g < 0 ) g = 0;
					if (b < 0 ) b = 0;
					if (r > 255 ) r = 255;
					if (g > 255 ) g = 255;
					if (b > 255 ) b = 255;

					pixels[offset] = r;
					pixels[offset+1] = g;
					pixels[offset+2] = b;
				}
			} while (--x);
		} while (--y);

		ctx.putImageData(imageData, 0, 0);
	}

	// Spocitat svetlo dopadajici na cip
	var computeLight = function() {
		return Math.log((shutter * ISO / (aperture * aperture)) / baseLight) / Math.LN2
	}

	// Inicializovat canvas
	this.initCanvas = function() {
		ctx.drawImage(backgroundImage, 0, 0, canvasWidth, canvasHeight);
		ctx.drawImage(foregroundImage, 0, 0, canvasWidth, canvasHeight);
	};

	// Vykreslit scenu
	this.render = function() {
		// Zvetseni: M = f / (x0 - f)
		var magnification = (focalLength * 1e-3) / (distance - focalLength * 1e-3);
		var zoom = magnification / baseZoom;

		var w = imageWidth / zoom;
		var h = imageHeight / zoom;
		var x = imageWidth / 2 - w / 2;
		var y = imageHeight / 2 - h / 2;

		ctx.drawImage(backgroundImage, x, y, w, h, 0, 0, canvasWidth, canvasHeight);

		// Blur disc: f * M / N
		// (N = clonove cislo)
		var blur = blurRadius * focalLength * magnification / aperture;
		boxBlurCanvasRGBA("canvas", 0, 0, canvasWidth, canvasHeight, blur, 1);

		// Vykreslit popredi
		ctx.drawImage(foregroundImage, x, y, w, h, 0, 0, canvasWidth, canvasHeight);

		if (!viewFinder) {
			// Osvetleni
			var light = computeLight();
			changeBrightness(light);

			// Sum na senzoru
			var noise = Math.log(ISO / baseISO) / Math.LN2;
			addNoise(0.9, 0.1 * noise);
		}
	};

	// Nastavit novou ohniskovou vzdalenost
	this.updateFocalLength = function(value) {
		focalLength = value;
		this.render();
	};

	// Nastavit novou hodnotu clony
	this.updateAperture = function(value) {
		aperture = value;
		this.render();
	};

	// Nastavit novou dobu expozice
	this.updateShutter = function(value) {
		shutter = value;
		this.render();
	};

	// Nastavit novou citlivost
	this.updateISO = function(value) {
		ISO = value;
		this.render();
	};

	// Nastavit zamek expozice
	this.setViewFinder = function(value) {
		viewFinder = value;
		this.render();
	}
}