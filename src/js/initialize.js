/**
 * Simulator digitalni zrcadlovky
 *
 * Projekt do predmetu Fyzikalni optika, FIT VUT 2013
 *
 * Jan Dusek, xdusek17@stud.fit.vutbr.cz
 * Vojtech Smejkal, xsmejk07@stud.fit.vutbr.cz
 */

function initSliders(cameraSim)
{
	$("#slider_focal_length").slider({
		from: 18,
		to: 55,
		skin: 'plastic',
		limits: false,
		dimension: '&nbsp;mm',
		scale: ['18', '|', '27', '|' , '37', '|', '46', '|', '55'],
		callback: function(value){
			cameraSim.updateFocalLength(value);
		}
	});

	$("#slider_aperture").slider({
		from: 0,
		to: 9,
		skin: 'plastic',
		limits: false,
		values: [1, 1.4, 2, 2.8, 4, 5.6, 8, 11, 16, 22],
		scale: ['f/1', '|', 'f/2', '|' , 'f/4', '|', 'f/8', '|', 'f/16', 'f/22'],
		calculate: function(i) {
			return "f/" + this.settings.values[i];
		},
		callback: function(i){
			var value = this.settings.values[i];
			cameraSim.updateAperture(value);
		}
	});

	$("#slider_shutter").slider({
		from: 0,
		to: 10,
		skin: 'plastic',
		limits: false,
		dimension: '&nbsp;s',
		values: ['1/2000', '1/1000', '1/500', '1/250', '1/125', '1/60', '1/30', '1/15', '1/4', '1/2', '1'],
		scale: ['1/2000', '|', '1/500', '|', '1/125', '|', '1/30', '|', '1/4', '|', '1'],
		calculate: function(i) {
			return this.settings.values[i];
		},
		callback: function(i){
			var value = eval(this.settings.values[i]);
			cameraSim.updateShutter(value);
		}
	});

	$("#slider_iso").slider({
		from: 0,
		to: 5,
		skin: 'plastic',
		limits: false,
		values: [100, 200, 400, 800, 1600, 3200],
		scale: ['100', '200', '400', '800', '1600', '3200'],
		calculate: function(i) {
			return this.settings.values[i];
		},
		callback: function(i){
			var value = this.settings.values[i];
			cameraSim.updateISO(value);
		}
	});
}


// Inicializovat UI
$(document).ready(function(){
	var camera = new CameraSim();
	initSliders(camera);

	$("#viewfinder").click(function(e) {
		var checked = e.target.checked;

		if (checked)
			$("#slider_aperture ~ .jslider ~ *").animate({opacity: 0.15}, 200);
		else
			$("#slider_aperture ~ .jslider ~ *").animate({opacity: 1}, 200);

		camera.setViewFinder(checked);
	});

	// Wait for images to load
	$(window).load(function(){
		camera.initCanvas();
		$("#controls_cont").css('visibility', 'visible');
	});
});