# DSLR simulator #

Digital single-lens reflex camera simulator written in Javascript and HTML.

Developed as university assignment to Physical Optics at FIT BUT in 2013.

[Demo](http://www.stud.fit.vutbr.cz/~xsmejk07/FYO-en/)